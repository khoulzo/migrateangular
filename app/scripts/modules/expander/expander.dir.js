angular.module('migrateApp')
  .directive('expander', function () {

    return {
       restrict: 'E',
       transclude: true,
       scope: {
         title: '='
       },
      templateUrl: 'scripts/modules/expander/expander.html',

      link: function (scope,element,attrs) {
        scope.showMe = false;
        scope.toogle = function toogle() {
          scope.showMe = !scope.showMe;
        }
      }

    };

  });
