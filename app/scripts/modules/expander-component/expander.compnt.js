angular.module('migrateApp')
  .component('expandercomp', {

      bindings : {
        title: '<'
      },

      transclude: true,

      templateUrl: 'scripts/modules/expander-component/expander.html',

      controller : function () {
        this.showMe = false;
        this.toogle = toogle;

        function toogle() {
          this.showMe = !this.showMe;
        }
      },

      controllerAs: 'expander'
  }
  );
