'use strict';

/**
 * @ngdoc function
 * @name migrateApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the migrateApp
 */
angular.module('migrateApp')
  .controller('MainCtrl', function () {

     var vm=this;
     vm.title ="Titre";
     vm.content = "Contenu";

  });
